<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tugas php-3</title>
</head>
<body>
<h1>Tentukan Nilai</h1>
    <?php
    function tentukan_nilai($number)
    {
        $output = " ";
        if ($number >= 85 && $number <= 100) {
            $output .= "Sangat Baik";
        }else if ($number >= 70 && $number <= 85) {
            $output .= "Baik";
        }else if ($number >= 60 && $number <= 70) {
            $output .= "Cukup";
        }else {
            $output .= "kurang";
        }
        return "$output <br>";
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>